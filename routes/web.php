<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LibroController;
use App\Http\Controllers\AyudaController;
use App\Http\Controllers\UsuarioLibrosController;


#Algunas pruebillas
Route::get('prueba', function () {
    return view ('pruebas.formu_prueba');
});

Route::get('audio', function () {
    return view ('pruebas.audio');
});

Route::get('/openlibra', function () {
    return view ('libros');
});

require __DIR__.'/auth.php';
