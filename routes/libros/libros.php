<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LibroController;
use App\Http\Controllers\GeneroController;

#Migrador con algunas tablas
Route::get('creageneros', [GeneroController::class, 'creaGeneros']);

#De los libros
Route::get('/', [LibroController::class, 'index']);
Route::get('/genero', [LibroController::class, 'listaPorGenero']);

#Buscador de Libros
Route::get('/buscaLibro', [LibroController::class, 'buscaLibro']);