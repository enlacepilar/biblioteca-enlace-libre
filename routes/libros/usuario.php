<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioLibrosController;

#Del usuario, sus libros y sus datos
Route::get('/datosUsuario', [UsuarioLibrosController::class, 'datosUsuario'])->middleware('auth');
Route::post('/modificaUsuario', [UsuarioLibrosController::class, 'modificaUsuario'])->middleware('auth');
Route::get('/usuario', [UsuarioLibrosController::class, 'index'])->middleware('auth');
Route::get('/ingresarLibro', [UsuarioLibrosController::class, 'ingresarLibro'])->middleware('auth');
Route::post('/recibeLibro', [UsuarioLibrosController::class, 'recibeLibro'])->middleware('auth');
Route::post('/modificaLibro', [UsuarioLibrosController::class, 'modificaLibro'])->middleware('auth');
Route::post('/actualizaLibro', [UsuarioLibrosController::class, 'actualizaLibro'])->middleware('auth');
Route::post('/eliminaLibro', [UsuarioLibrosController::class, 'eliminaLibro'])->middleware('auth');
