<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AyudaController;

   
#De la ayuda y otros datos
Route::get('/proyecto', [AyudaController::class, 'index']);
Route::get('/edita', [AyudaController::class, 'edita']);
Route::get('/colabora', [AyudaController::class, 'colabora']);
Route::get('/cultura-libre', [AyudaController::class, 'culturaLibre']);
