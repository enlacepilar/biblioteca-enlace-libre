@extends('layouts.app')
@section('contenido_app')

<div style="background-color:white;" class="container-fluid mt-3 animate__animated animate__zoomIn" id='contenedor_formu'>
    <div><img src="imagenes/colabora.png" alt="Escudo" width="25%"></div> 
    <div id='tabla_formu' class='mt-2 mb-2'>
            <table id='tabla_interna'>
                <tr><td colspan="2" id='titulo_formu'>Formulario de Alta de Usuario</td></tr>
                <tr><td id='items-firma1'>Fecha: </td><td><script>
                    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                    var f=new Date();
                    document.write(f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());</script></td></tr>
                <tr><td colspan="2" id='titulos_internos'>Datos del usuario:</td></tr>
                <tr><td id='items-firma1'>Organismo: </td><td id='items-firma2'>Texto de prueba</td></tr>
                <tr><td id='items-firma1'>Nombre: </td><td id='items-firma2'>texto de prueba</td></tr>
                <tr><td id='items-firma1'>Apellido: </td><td id='items-firma2'>atexto de prueba</td></tr>
                <tr><td id='items-firma1'>Cargo: </td><td id='items-firma2'>texto de prueba</td></tr>
                <tr><td id='items-firma1'>DNI: </td><td id='items-firma2'>d</td></tr>
                <tr><td id='items-firma1'>Legajo: </td><td id='items-firma2'>ltexto de prueba</td></tr>
                <tr><td id='items-firma1'>Correo-e: </td><td id='items-firma2'>texto de prueba</td></tr>
                <tr><td id='items-firma1'>Tel./Interno: </td><td id='items-firma2'>t</td></tr>
            
                <tr><td colspan="2"  id='titulos_internos'>prueba enlacePilar</td></tr>
                <tr><td colspan="2" id='roles' style="height:200px;">
                Nada de ROLESSSS
                </td></tr>
               
                <div id='problema'> </div>
                <tr><td colspan="2" id='titulos_internos'>Autorización:</td></tr>
            </table>
            <table id='tabla_formu'>
                <tr><th id='items-firma3'>Firma TITULAR  </th><th>Aclaración:</th></tr>
                <tr><td id='espacio-firma'></td><td></td></tr>
            </table>

    
    </div>
    <div id='imprimir'><button class='btn btn-danger btn-block' onclick='imprime()'>Imprimir</button></div> 
</div>
   
   
<script src="html2pdf/html2pdf.bundle.min.js"></script>


<script>
let imprime = ()=>
{

    const $elementoParaConvertir = document.body; // <-- Aquí puedes elegir cualquier elemento del DOM
    html2pdf()
        .set({
            margin: 1,
            filename: 'documento.pdf',
            image: {
                type: 'jpeg',
                quality: 0.98
            },
            html2canvas: {
                scale: 3, // A mayor escala, mejores gráficos, pero más peso
                letterRendering: true,
            },
            jsPDF: {
                unit: "in",
                format: "a3",
                orientation: 'portrait' // landscape o portrait
            }
        })
        .from($elementoParaConvertir)
        .save()
        .catch(err => console.log(err));
}
</script>
@endsection