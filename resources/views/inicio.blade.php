@extends('layouts.app')
@section('contenido_app')
    <div class="alert-warning m-3 p-3">
        <h5 align="center">Bienvenido a la Biblioteca de publicaciones con licencias libres.</h5>
    </div>
    
    @include('libros.buscador')

    <img src="/imagenes/ultimos-libros.png" class="img-fluid" alt="Ultimos Libros Agregados">
    <hr>
    <div class="row m-3 p-3">
       
        <section class="text-center">
            @foreach ($libros as $libro )
            <div class="card p-3 m-1 librosPrincipal">
                <img src="{{$libro->imagen}}" class="card-img-top" width="250px" alt="Tapa Libro">
                <div class="card-body">
                    <h5 class="card-title">{{$libro->titulo}}</h5>
                    <h6 class="card-title">{{$libro->autor}}</h6>
                    <p class="card-text">{{$libro->descripcion}}</p>
                    <a href="{{$libro->enlace_libro}}" target="_blank" class="btn btn-success">Descargar</a>
                </div>

            </div>
            @endforeach
        
           
        
        </section>
        <div class="text-center">{{$libros->links()}}</div>
        
   
    </div>
   
    

    <br><br>
    <section>
        <div class="botonera_y_paginacion">
            <form action="genero" method="GET">
                @csrf
                @foreach ($generos as $genero )
                    <button type="submit" value="{{$genero->id}}" name="genero" class="btn btn-outline-secondary m-1">{{$genero->nombre}}</button>                    
                @endforeach
            </form>
        </div>
    </section>

@endsection