
<!-- DATATABLES -->
<script src="/Datatables/jquery.dataTables.min.js"></script>
<script src="/Datatables/dataTables.buttons.min.js"></script>
<script src="/Datatables/jszip.min.js"></script>
<script src="/Datatables/pdfmake.min.js"></script>
<script src="/Datatables/vfs_fonts.js"></script>
<script src="/Datatables/buttons.html5.min.js"></script>
<script src="/Datatables/buttons.print.min.js"></script>

<!-- #endregion -->
@include('usuario.script_datatables')