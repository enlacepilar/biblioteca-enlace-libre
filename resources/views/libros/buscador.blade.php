<div class="p-4 m-3">
    <form action="buscaLibro" method="GET">
        @csrf
        <div class="row p-3 alert-info" style="border: 2px solid black">
            <div class="col-2">
                <select name="tituloOAutor" id="" class="form-control">
                    <option value="titulo">Título</option>
                    <option value="autor">Autor</option>
                </select>
            </div>
            <div class="col-4">
                <input type="text" name="buscaLibro" id="buscaLibro" class="form-control" placeholder="Buscar libro" required>
            </div>
            <div class="col-4">
                <select name="buscaGenero" id="" class="form-control">
                    <option value="">Seleccione género</option>
                    @foreach ($generos as $genero )
                        <option value="{{$genero->id}}">{{$genero->nombre}}</option>                
                    @endforeach
                </select>
            </div>
            <div class="col-2">
                <input type="submit" value="Buscar" id="" class="btn btn-info">
            </div>
        </div>
    </form>

</div>