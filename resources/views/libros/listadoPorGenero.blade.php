@extends('layouts.app')
@section('contenido_app')

<div class="m-5 p-3">
    <h1>Listado de libros por género</h1>
    @if (isset($buscaLibro))
        <p>Buscando libro que responda al término: {{$buscaLibro}}</p>
    @endif
    <div>
        @if (count($libros) != 0)
            <div class="table-responsive">
            <table class="table table-info table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-center">
                        <th>Género</th>
                        <th>Fecha Alta:</th>
                        <th>Titulo</th>
                        <th>Descripción</th>
                        <th>Autor</th>
                        <th>Subido por:</th>
                        <th>Tapa</th>
                        <th>Descargar</th>
                    </tr>
                </thead>
                <tbody>

                @foreach ($libros as $libro)
                    <tr>
                        @if (isset($libro->genero_id))
                            <td>{{$libro->generoLibro->nombre}}</td>    
                        @else
                            <td>Sin Genero</td>
                        @endif
                   
                        <td>{{date("d/m/Y", strtotime($libro->created_at))}}</td>
                        <td>{{$libro->titulo}}</td>
                        <td>{{$libro->descripcion}}</td>
                        <td>{{$libro->autor}}</td>
                        <td>{{$libro->user->name}}</td>
                        <td><img src="{{$libro->imagen}}" width="200px" alt="Libro"></td>
                        <td><a href="{{$libro->enlace_libro}}" target="_blank"><button class="btn btn-danger">Descargar</button></a></td>
                    </tr>
                    @endforeach
                </tbody>        
            </table>
            </div>
            <b>Resultados: {{$libros->total()}}</b>
            <br>
            @if (isset($buscaLibro))
                {{$libros->appends(['buscaLibro' => $buscaLibro, 'buscaGenero' => $buscaGenero, 'tituloOAutor'=>$tituloOAutor])}} 
            @else
                {{$libros->links()}}

            @endif

        @else
            <h4>Sin resultados para la categoria seleccionada</h4>
        @endif
    </div>


</div>

@endsection