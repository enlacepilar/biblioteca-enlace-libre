<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- link a bootstrap, sweetalert2  y Datatables-->
    <link rel="stylesheet" href="/bootstrap-local/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="bootstrap/PULSE-bootstrap.min.css"> -->
    <link rel="stylesheet" href="/sweetAlert2/sweetalert2.min.css">
    <link rel="stylesheet" href="/animatecss/animate.min.css">

    <title>Document</title>
</head>
<body>
    buscar en Open Libra <input type="text" id="busca" class="form-control">  
    <button class="btn btn-info" onclick="consultaOpenLibra()">Consultar</button>

    <div id="resultados"></div>

  <!-- scripts a Jquery, bootstrap, sweetAlert2 y DataTables  -->
  <script src="/js/jquery-3.5.1.min.js"></script>
  <script src="/bootstrap-local/js/bootstrap.min.js"></script>
  <script src="/sweetAlert2/sweetalert2.all.min.js"></script>

    <script>
        let consultaOpenLibra = async ()=>
        {
            let titulo = document.getElementById('busca').value
            console.log ("busco: " +titulo)
            const url = 'http://www.etnassoft.com/api/v1/get/?book_title='+titulo
            // const request = new Request(url, 
            // {
            //     method: 'GET',
            //     //body: JSON.stringify({"idLibro": idLibro}),
            //     cache: "no-cache",
            //     //headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            
            // });
            const response = await fetch(url);
            const data = await response.json();       
            console.log(data)
            // Swal.fire(
            //     {
            //         type: 'success',
            //         title: data,
            //         showConfirmButton: false,
            //         timer: 1500
            //     })    
                
        }
        </script>
</body>
</html>