@extends('layouts.app')
@section('contenido_app')
<div class="container mt-4">
    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif
    <h1>Modificar datos del usuario</h1>
    
    <form action="/modificaUsuario" method="POST">
    @csrf
        @foreach($usuario as $usu)
            <input type="hidden" name="id" value="{{$usu->id}}">
            <label for="nombre">Nombre:</label>
            <input type="text" id="nombre" name="nombre" class="form-control" required value="{{$usu->name}}"> 
            <br>
            <label for="correo">Correo:</label>
            <input type="text" id="correo" name="correo" class="form-control" required value="{{$usu->email}}"> 
        @endforeach
        <br>
        <input type="submit" value="Actualizar" class="btn btn-warning">
    </form>
   
</div>
@endsection
