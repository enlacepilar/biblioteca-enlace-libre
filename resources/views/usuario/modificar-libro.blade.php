@extends('layouts.app')
@section('contenido_app')
<div class="container mt-4">
    @if (isset($datos))
        @include('resultados.cargaBien')
        <a href="/usuario">Volver</a> | <a href="/">Inicio</a>
    @endif
    
    <h1>Modificar info del Libro</h1>
    <form action="/actualizaLibro" method="POST">
    @csrf
       
            <input type="hidden" name="id" value="{{$libro->id}}">
            <label for="titulo">Título:</label>
            <input type="text" id="titulo" name="titulo" class="form-control" required value="{{$libro->titulo}}"> 
            <br>
            
            <label for="genero">Modificar el género del libro. Actual: {{$libro->generoLibro->nombre}}</label>
        <select name="genero_id" id="genero" class="form-control">
            @foreach ($generos as $genero )
                <option value="{{$genero->id}}">{{$genero->nombre}}</option>
            @endforeach
        </select>
                
            
        <br>
            <label for="autor">Autor:</label>
            <input type="text" id="autor" name="autor" class="form-control" required value="{{$libro->autor}}"> 
            <br>
            <label for="descripcion">Descripción:</label>
            <textarea name="descripcion" id="descripcion" cols="100" rows="5" class="form-control">{{$libro->descripcion}}</textarea>
            
        <br>
            <input type="submit" value="¡Actualizar!" class="alert-warning btn btn-warning btn-block">
    </form>
   
</div>
@endsection
