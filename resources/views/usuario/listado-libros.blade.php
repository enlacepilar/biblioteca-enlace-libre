@extends('layouts.app')
@section('contenido_app')
<div class="container mt-4">
    <h1>Estás en la sección del usuario</h1>

    <div class="text-center"><a href="/ingresarLibro" class="btn btn-outline-primary"><i class="fas fa-pen-alt"></i> Ingresar un libro nuevo</a></div>
    <div class="table-responsive mt-3">
        <table class="table table-info table-bordered table-hover">
            <thead>
                <tr class="text-center table-primary">
                    <th>Fecha Alta:</th>
                    <th>Título</th>
                    <th>Género</th>
                    <th>Descripción</th>
                    <th>Autor</th>
                    <th>Tapa</th>
                    <th>Descargar</th>
                    <th>Modificar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($libros as $libro )
                <tr>
                    <td>{{date("d/m/Y", strtotime($libro->created_at))}}</td>
                    <td>{{$libro->titulo}}</td>
                    <td>{{$libro->generoLibro->nombre}}</td>
                    <td>{{$libro->descripcion}}</td>
                    <td>{{$libro->autor}}</td>
                    <td><img src="{{$libro->imagen}}" width="200px" alt="Libro"></td>
                    <td class="text-center"><a href="{{$libro->enlace_libro}}"></a><button class="btn btn-success"><i class="fas fa-cloud-download-alt"></i></button></td>
                <form action="/modificaLibro" method="POST">
                @csrf
                    <input type="hidden" name="idLibro" value="{{$libro->id}}" id="eliminaLibro">
                    <td class="text-center"><button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i></button></td>
                
                </form>
                    <td class="text-center"> <button class="btn btn-danger" onclick="eliminaLibro('{{$libro->id}}')"><i class="far fa-trash-alt"></i></button></td>
                @endforeach   
                    </tr>
            </tbody>
        </table>
        <b>Resultados: {{$libros->total()}}</b>
            <br>
        <div class="text-center"> {{$libros->links()}} </div>
    </div>
</div>
<script>
let eliminaLibro = async (idLibro)=>
{
    console.log ("El resultado es" + idLibro)
    if (confirm("¿Seguro que querés borrar el libro?"))
    {
        const url = '/eliminaLibro'
        const request = new Request(url, 
        {
            method: 'POST',
            body: JSON.stringify({"idLibro": idLibro}),
            cache: "no-cache",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        
        });
        const response = await fetch(request);
        const data = await response.json();       
        console.log(data)
        Swal.fire(
            {
                type: 'success',
                title: data,
                showConfirmButton: false,
                timer: 1500
            })
        location.reload()
        
    }
    
        
}
</script>
@endsection
