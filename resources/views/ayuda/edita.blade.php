@extends('layouts.app')
@section('contenido_app')
<div class="container mt-4">
    <h1>¿Cómo armo mi libro para compartir?</h1>
    <p>1) Primero y principal, pueden suceder varias cosas, en este ejemplo trato solo dos posibilidades.
        La primera, que tengas varios archivos de texto con los que venías armando tu obra.
        Entonces reuní todas esas hojas que tenes desperdigadas por ahí en un único archivo. 
        O también, otro caso podría ser que tenés que tipear alguna cosa que tenés en papel, claro. 
        Para el primer caso pintá el texto con el mouse, y vas a copiar e ir pegando en ese mismo documento todos textos
        para armar un único archivo. Para el segundo... ¡a tipear! Luego de ese largo proceso, repetí el caso primero o bien pasá 
        al punto dos, donde tipéas directamente en un procesador como el LibreOffice Writer. Va en gustos.
    </p>
    <img src="/imagenes/edita/edita00.jpg" alt="Abrir programa" srcset="">
    <br>
    <p>2) Pasalo a un editor de texto como LibreOffice Writer. </p>
    <img src="/imagenes/edita/edita01.jpg" alt="Abrir programa" srcset="">
    <br>
    <p>3) Ajustá el párrafo como te parezca más conveniente, pintando el texto completo o por párrafo. Como sugerencia podés ir a párrafo,
        flujo te texto y dividir con guiones. Además de eso, el texto puede ir justificado y los títulos un poco más grandes
    que el resto del texto. </p>
    <img src="/imagenes/edita/edita03.jpg" alt="Formato párrafo" srcset="">
    <br>
    <p>4) Finalmente, cuando tengas ya todo editado, exportalo a PDF desde ARCHIVO, EXPORTAR A, PDF</p>
    <img src="/imagenes/edita/edita03.jpg" alt="Exportar a PDF" srcset="">
    <br><hr>
    <h5>Si esto no te funcionó o no se te da bien armarte el libro, ¡<a href="mailto:enlacepilar@protonmail.com">escribime</a> y te doy una mano!</h5>
    
    
</div>
@endsection