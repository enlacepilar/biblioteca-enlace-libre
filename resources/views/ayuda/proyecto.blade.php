@extends('layouts.app')
@section('contenido_app')
<div class="container mt-4">
    <img src="/imagenes/acerca-de-este-proyecto1.png" class="img-fluid" alt="Acerca de este proyecto">
    <div class="alert-warning p-4">
        <p>Bienvenido a Biblioteca EnlaceLibre. Este proyecto surgió a partir de la materia "Culturas Digitales Libres" de la 
        Universidad Nacional del Litoral, donde me encuentro realizado la Tecnicatura en Software Libre.</p>
        <p>Está montado en una Raspberri Pi y realizado en en el marco de trabajo Laravel de PHP. Por lo que te puede llegar 
            a pasar que a veces esté fuera de línea por algún motivo local. No sucede mucho, pero no pierdas las esperanzas de 
            volver a acceder.
        </p>
        <h4>¡Compartir, compartir, compartir!</h4>
        <p>La idea es reunir en este sitio textos en formato PDF con licencias CC u otras licencias no restrictivas, es decir,
        que se puedan descargar libremente y puedan ser, si así se deseara por un lector, poder ser compartidos
        o remixados sin restricciones. </p>
        <p>Es por ello que las licencias tenidas en cuenta para subir al sitio serían, valga la 
        restricción con <b>fines positivos (entiéndase fines positivos como compartirlo todo, la base para una cultura libre)</b>:</p>
        <p><img src="https://www.creativecommons.org.ar/media/uploads/licencias/by-125px.png" alt="Licencia Atribucion"> <b>Atribución (by)</b>: Se permite cualquier explotación de la obra, incluyendo la explotación con fines comerciales y la creación de obras derivadas, la distribución de las cuales también está permitida sin ninguna restricción. Esta licencia es una licencia libre según la Freedom Defined.</p>
        <p><img src="https://www.creativecommons.org.ar/media/uploads/licencias/by-sa-125px.png" alt="Licencia Reconocimiento"><b>Reconocimiento – Compartir Igual (by-sa)</b>: Se permite el uso comercial de la obra y de las posibles obras derivadas, la distribución de las cuales se debe hacer con una licencia igual a la que regula la obra original. Esta licencia es una licencia libre según la Freedom Defined.</p>

    
        <a href="https://creativecommons.org.ar/licencias/" target="_blank">Más info sobre las licencias Creative Commons</a>
    <br><hr>
        <p>Como mi conocimiento sobre licencias libres es limitado, existe la posibilidad de que 
            conozcas otro tipo de licencias que permitan compartir cultura por este medio. Si llegase a ser
            de ese modo no dudes en escribirme para que pueda ser agregado por acá.
            <a href="mailto:enlacepilar@protonmail.com">Mi correo.</a>
        </p>
    <img src="/imagenes/biblioteca-enlacelibre-efectoss.jpg" alt="Mi super equipo">
    <hr>
    <p>Actualmente tengo en mente dos proyectos: el primero es tener este mismo proyecto fuera de línea en una pc cualquiera, cosa que tengo 
         bastante avanzada y para ello me basé en la lectura del proyecto <a href="https://bibliobox.copiona.com/" target="_blank">Bibliobox</a> 
         (gracias profes Constanza y Berna que me recomendaron el sitio). Más adelante me encantaría poder hacer un tuto explicativo en PDF; y el segundo sería
        que este respositorio pueda venir con un script autoinstalable, de manera tal que con un simple script montes
    Apache, MariaDB, te cree el usuario y después la cosa es más sencilla, porque Laravel te migra las bases de manera automática. Veremos...  </p>
    </div>
</div>
@endsection