@extends('layouts.app')
@section('contenido_app')
<div class="container mt-4">
    <h2>Cultura Libre</h2>
    {{-- <img src="/imagenes/colabora02.png" class="img-fluid" alt="Colaborá"> --}}
    <div class="alert-warning p-4">
       <p><b><i>¿Qué es Cultura Libre?</i></b></p>
        <p>Podría comenzar diciendo que si buscás en internet vas a encontrar muchas definiciones que puedan satisfacerte. Es 
            por ello que me permito algunas licencias a la hora de definir y por qué no, ciertas vaguedades y pensamientos no muy formales :)
            En este caso quisiera definir la cultura libre sólo para los libros digitales: vendría a ser como una forma de compartir sin restricciones y 
            con la finalidad de que puedas hacer y disponer con la obra lo que te guste, sin temor a ser <i>"perseguido"</i> o con 
            la idea de estar cometiendo alguna infracción ficticia como las que gesta la cultura de la propiedad intelectual. 
            Al menos en este sitio, todo el material que descargues va a guardar ese fin. 
        </p>
        <p>
            Es por ello que desde acá alentamos la cultura de compartir, como sinónimo de cultura libre. Si tenes un libro hecho por vos,
            no importa cómo esté diseñado, capaz son unos esbozos en un procesador de texto, subilos igual, alguien va a disfrutar de su lectura, y 
            quién sabe, a lo mejor lo modifica y colabora y hace otra obra, o le agrega fotos... las posibilidades son infinitas compartiendo y permitiendo.
        </p>
       
    </div>
</div>
@endsection