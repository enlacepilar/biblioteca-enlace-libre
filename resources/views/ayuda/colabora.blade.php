@extends('layouts.app')
@section('contenido_app')
<div class="container mt-4">
    <img src="/imagenes/colabora02.png" class="img-fluid" alt="Colaborá">
    <div class="alert-warning p-4">
        <p>Este sitio se encuentra realizado en Laravel y liberado todo su código en GiLab.</p> 
        <p> Para clonarlo o realizar mejoras, hacé clic <a href="https://gitlab.com/enlacepilar/biblioteca-enlace-libre.git" target="_blank">acá</a> </p>
        <p>Sos libre de (basado en las 4 libertades del Software Libre):</p>
        <p>-Clonar el repositorio y usarlo con cualquier propósito.
        <p>-Poder estudiar el código y modificarlo.</p>
        <p>-¡Poder compartir el repositorio o avisarle a alguien más!</p>
        <p>-Poder mejorar el sitio y poder compartir dichas mejoras para beneficio de todos.</p>
        <br>
        <hr>
        <p>Puse una nota sobre este sitio <a href="https://www.tecnico-ligero.tech/2021/11/29/biblioteca-enlacelibre/">acá.</a></p>
        <hr>
        <p>Si querés colaborar en este proyecto, <a href="mailto:enlacepilar@protonmail.com">escribime.</a></p>
    </div>
</div>
@endsection