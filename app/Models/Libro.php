<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    use HasFactory;

    //nota IMPORTANTE, LA COLUMNA CON OTRO NOMBRE ES DE LA TABLA LIBRO
    public function user ()
    {
        return $this->belongsTo(User::class, 'usuario');
    }

    //O sea, la columna con la llave foranea apuntando a GENERO, o sea genero_id
    public function generoLibro() //lo pongo asi porque se confunde con la columna y da error
    {
        return $this->belongsTo(Genero::class, 'genero_id');
    }

}
