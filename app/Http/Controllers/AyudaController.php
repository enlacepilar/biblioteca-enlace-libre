<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genero;

class AyudaController extends Controller
{
    public function traeGeneros()
    {
        $generos = Genero::all();
        return $generos;
    }

    public function index()
    {
        return view('ayuda.proyecto', ['generos'=>$this->traeGeneros()]);
    }

    public function edita()
    {
        return view('ayuda.edita', ['generos'=>$this->traeGeneros()]);
    }

    public function colabora()
    {
        return view('ayuda.colabora', ['generos'=>$this->traeGeneros()]);
    }

    public function culturaLibre()
    {
        return view('ayuda.cultura-libre', ['generos'=>$this->traeGeneros()]);
    }
}
