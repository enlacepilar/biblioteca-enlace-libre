<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genero;

class GeneroController extends Controller
{
    public function creaGeneros ()
    {
        $generos = Genero::all();

        if (count($generos)<1)
        {
            $genero = new Genero;
            $genero->nombre = "Novela";
            $genero->save();

            $genero = new Genero;
            $genero->nombre = "Cuento";
            $genero->save();

            $genero = new Genero;
            $genero->nombre = "Ensayo";
            $genero->save();

            $genero = new Genero;
            $genero->nombre = "Poesía";
            $genero->save();

            echo "Géneros creados exitosamente";
        }
        else{
            echo "Los géneros ya fueron creados anteriormente.";
        }
    }
}
