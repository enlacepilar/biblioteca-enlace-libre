<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Libro;
use App\Models\Genero;

class UsuarioLibrosController extends Controller
{
    public function traeGeneros()
    {
        $generos = Genero::all();
        return $generos;
    }

    public function index()
    {
        $usuarioID = auth()->id();
    
        $libros = Libro::where('usuario', $usuarioID)->paginate(15);
        
        return view('usuario.listado-libros', ['libros'=>$libros, 'generos'=>$this->traeGeneros()]);
    }

    public function ingresarLibro()
    {
        return view ('usuario.ingresarLibro', ['generos'=>$this->traeGeneros()]);
    }

    public function recibeLibro(Request $request)
    {
        $request->validate([
            'imagen'=>'image|max:1024|mimes:jpeg,png,jpg',
            'enlace_libro'=>'required|file|max:20000|mimes:pdf',
            'descripcion'=> 'max:255'
        ]);

        $usuarioID = auth()->id();
        $libroNuevo = request()->except('_token');

      
        if (count($libroNuevo) == 6) //esta es la cantidad del array SI SUBE imagen
        {
            $nombreImagen = rand (0, 999) . $request->file('imagen')->getClientOriginalName();
            $urlImagen = "/storage/libros_imagenes/". $nombreImagen;
            $request->file('imagen')->storeAs('public/libros_imagenes', $nombreImagen);
            $libroNuevo['imagen']= $urlImagen;
        }else{
            $libroNuevo['imagen']="/imagenes/sin-imagen.jpg";
        }
       
        if ($libroNuevo['enlace_libro'] != null)
        {
            $nombreLibro = rand (0, 999) . $request->file('enlace_libro')->getClientOriginalName();
            $urlLibro = "/storage/libros_pdf/". $nombreLibro;
            $request->file('enlace_libro')->storeAs('public/libros_pdf', $nombreLibro);
            $libroNuevo['enlace_libro']  = $urlLibro;
        }else{
            return "El libro es obligatorio!";
        }
        $libroNuevo['usuario'] = $usuarioID;
        $libroNuevo['created_at'] = date('Y-m-d H:i:s');
        Libro::insert($libroNuevo);
            
        $datos = "El libro que cargaste fue ingresado a la base de datos :)";
        return view('usuario.ingresarLibro', ['datos' => $datos, 'generos'=>$this->traeGeneros()]);
    
    }

    public function datosUsuario ()
    {
        $usuarioID = auth()->id();
        $usuario = User::where('id', $usuarioID)->get();
        return view('usuario.modificar-datos-usuario', ['usuario'=>$usuario, 'generos'=>$this->traeGeneros()]);
    }

    public function modificaUsuario (Request $request)
    {
        $id = $request->get('id');
        $nombre = $request->get('nombre');
        $correo = $request->get('correo');

        $actualizaUsu = User::find($id);
        $actualizaUsu->name = $nombre;
        $actualizaUsu->email = $correo;
        $actualizaUsu->save(); 

        $usuarioID = auth()->id();
        $usuario = User::where('id', $usuarioID)->get();
        return view('usuario.modificar-datos-usuario', ['usuario'=>$usuario, 'datos'=>"Datos actualizados.", 'generos'=>$this->traeGeneros()]);
        
    }

    public function modificaLibro (Request $request)
    {
        $idLibro = $request->get('idLibro');
        $libro = Libro::find($idLibro);
        return view ("usuario.modificar-libro", ["libro"=>$libro, 'generos'=>$this->traeGeneros()]);
    }

    public function actualizaLibro (Request $request)
    {
        $libro = Libro::find($request->id);
   
        $libro->titulo = $request->titulo;
        $libro->genero_id = $request->genero_id;
        $libro->autor = $request->autor;
        $libro->descripcion = $request->descripcion;
        $libro->save();
        
        $datos = "Actualizaste la info del libro.";
        $libro = Libro::find($request->id);
        return view ("usuario.modificar-libro", ["libro"=>$libro, "datos"=>$datos, 'generos'=>$this->traeGeneros()]);
    }

    public function eliminaLibro (Request $request)
    {
        $idLibro = $request->json('idLibro');
        $libro = Libro::find($idLibro);
        $urlRAIZ = getcwd();

        try
        {
            if(stristr($libro->imagen, 'sin-imagen.jpg')) 
            {
                unlink ($urlRAIZ . $libro->enlace_libro);
                Libro::destroy($idLibro);
                return response()->json("Libro borrado");
            }
            else
            {
                unlink ($urlRAIZ . $libro->enlace_libro);
                unlink ($urlRAIZ . $libro->imagen);
                Libro::destroy($idLibro);
                return response()->json("Borrados libro y tapa");
            }
        }catch (Exception $e){
            echo ("¡¡¡Error!!! <br> <b>" . $e->getMessage() . "</b>");
        }    
    }
}
