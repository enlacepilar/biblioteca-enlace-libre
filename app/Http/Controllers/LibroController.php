<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Libro;
use App\Models\Genero;


class LibroController extends Controller
{
    public $paginacion = 15;

    public function index()
    {
        $libros = Libro::latest()->paginate(6);
        $generos = Genero::all();
        return view('inicio', ['libros'=>$libros, 'generos'=>$generos]);
    }

    public function listaPorGenero(Request $request)
    {
        $genero = $request->get('genero');
        $libros = Libro::where('genero_id', $genero)
        ->orderBy('created_at', 'desc')
        ->paginate($this->paginacion);
        
        $generos = Genero::all();

        return view('libros.listadoPorGenero', ["libros"=>$libros, 'generos'=>$generos]);
    }

    public function buscaLibro(Request $request)
    {
        $buscaLibro = $request->get('buscaLibro');
        $buscaGenero = $request->get('buscaGenero');
        $tituloOAutor = $request->get('tituloOAutor');

        //dd($tituloOAutor);

        if ($buscaGenero == "")
        {
            $libros = Libro::where("$tituloOAutor", 'LIKE', "%$buscaLibro%")
                ->paginate($this->paginacion);
                //dd($libros);
        }else{
            $libros = Libro::where("$tituloOAutor", 'LIKE', "%$buscaLibro%")
                ->where('genero_id', $buscaGenero)
                ->paginate($this->paginacion);
                //dd($libros);
        }

        $generos = Genero::all();

        return view('libros.listadoPorGenero', ["libros"=>$libros, 'generos'=>$generos, 
            'buscaLibro' => $buscaLibro, 'buscaGenero' => $buscaGenero, 'tituloOAutor'=>$tituloOAutor
        ]);
    }

}